<?php
/*
Plugin Name: TinyMCEditor Custom Styles
Version:  0.1.6
Plugin URI: http://www.speckygeek.com
Description: Add custom styles in your posts and pages content using TinyMCE WYSIWYG editor. The plugin adds a Styles dropdown menu in the visual post editor.
Based on TinyMCE Kit plug-in for WordPress
http://plugins.svn.wordpress.org/tinymce-advanced/branches/tinymce-kit/tinymce-kit.php
http://alisothegeek.com/2011/05/tinymce-styles-dropdown-wordpress-visual-editor/
* Bitbucket Plugin URI: https://bicarbona:lucuska@bitbucket.org/bicarbona/editor-custom-style
* Bitbucket Branch: master

*/
/**
 * Apply styles to the visual editor
 */ 
add_filter('mce_css', 'tuts_mcekit_editor_style');
function tuts_mcekit_editor_style($url) {
 
    if ( !empty($url) )
        $url .= ',';
 
    // Retrieves the plugin directory URL
    // Change the path here if using different directories
    $url .= trailingslashit( plugin_dir_url(__FILE__) ) . '/editor-styles.css';
 
    return $url;
}
 
/**
 * Add "Styles" drop-down
 */
add_filter( 'mce_buttons_2', 'tuts_mce_editor_buttons' );
 
function tuts_mce_editor_buttons( $buttons ) {
    array_unshift( $buttons, 'styleselect' );
    return $buttons;
}
 
/**
 * Add styles/classes to the "Styles" drop-down
 */
add_filter( 'tiny_mce_before_init', 'tuts_mce_before_init' );
 
function tuts_mce_before_init( $settings ) {
 
    $style_formats = array(
        array(
            'title' => 'Download Link',
            'selector' => 'a',
            'classes' => 'download'
            ),
        array(
            'title' => 'Testimonial',
            'selector' => 'p',
            'classes' => 'testimonial',
        ),
        array(
            'title' => 'Warning Box',
            'block' => 'div',
            'classes' => 'warning box',
            'wrapper' => true
        ),
		
        array(
            'title' => 'UL disc', 
            'selector' => 'ul', 
            'classes' => 'disc' 
        ),
        array(
            'title' => 'UL square', 
            'selector' => 'ul', 
            'classes' => 'square'
        )
        ,
        array(
            'title' => 'IMG circle', 
            'selector' => 'img', 
            'classes' => 'img-circle'
        ),
        array(
        'title' => 'Animate -Fade In',
        'selector' => '*',
        'classes' => 'animated fadeIn',

      ),

      array(
        'title' => 'Animate - Fade In Left',
        'selector' => '*',
        'classes' => 'animated fadeInLeft',

      ),
      array(
        'title' => 'Animate - Fade In Right',
        'selector' => '*',
        'classes' => 'animated fadeInRight',

      ),
	  

      array(
       'title' => 'Uppercase',
       'selector' => '*',
       'classes' => 'uppercase',

     ),
        array(
            'title' => 'Red Uppercase Text',
            'inline' => 'span',
            'styles' => array(
                'color' => '#ff0000',
                'fontWeight' => 'bold',
                'textTransform' => 'uppercase'
            ),
        )
    );
 
    $settings['style_formats'] = json_encode( $style_formats );
 
    return $settings;
 
}
 
/* Learn TinyMCE style format options at http://www.tinymce.com/wiki.php/Configuration:formats */
 
/*
 * Add custom stylesheet to the website front-end with hook 'wp_enqueue_scripts'
 */
add_action('wp_enqueue_scripts', 'tuts_mcekit_editor_enqueue');
 
/*
 * Enqueue stylesheet, if it exists.
 */
function tuts_mcekit_editor_enqueue() {
  $StyleUrl = plugin_dir_url(__FILE__).'editor-styles.css'; // Customstyle.css is relative to the current file
  wp_enqueue_style( 'myCustomStyles', $StyleUrl );
}

/* Style Format Options

title [required]              label for this dropdown item

selector|block|inline         selector limits the style to a specific HTML
[required]                    tag, and will apply the style to an existing tag
                              instead of creating one
                              
                              block creates a new block-level element with the
                              style applied, and will replace the existing block
                              element around the cursor
                              
                              inline creates a new inline element with the style
                              applied, and will wrap whatever is selected in the
                              editor, not replacing any tags

classes [optional]            space-separated list of classes to apply to the
                              element

styles [optional]             array of inline styles to apply to the element
                              (two-word attributes, like font-weight, are written
                              in Javascript-friendly camel case: fontWeight)

attributes [optional]         assigns attributes to the element (same syntax as styles)

wrapper [optional,            if set to true, creates a new block-level element
default = false]              around any selected block-level elements

exact [optional,              disables the “merge similar styles” feature, needed
default = false]              for some CSS inheritance issues

*/
?>